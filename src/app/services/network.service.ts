import { Injectable } from "@angular/core";
import { Http, Request, Headers } from "@angular/http";
// import "rxjs/add/operator/map";
// import "rxjs/Rx";
import "rxjs/add/operator/map";
@Injectable({
  providedIn: "root"
})
export class NetworkService {
  constructor(private http: Http) {}

  // defaultHeaders: any = {
  //   "Content-Type": "application/json; charset=utf-8",
  //   Accept: "application/json"
  // };
  url: string = "https://app-gain-be.herokuapp.com";
  // window.location.href.indexOf("localhost") !== -1
  //   ? "http://localhost:3000"
  //   : "http://dummy.restapiexample.com";
  // url: string = 'http://localhost:3000';
  // url: string = 'http://10.0.0.47:3000';
  private services = {
    testHttp: {
      url: "/mock",
      type: "GET"
    }
  };

  send(serviceName: any, options: any): Promise<any> {
    var service = Object.assign({}, this.services[serviceName]);
    var k, afterReplace, v;
    afterReplace = "";
    for (k in options) {
      v = options[k];
      afterReplace = service.url.replace(new RegExp("{" + k + "}", "m"), v);
      if (service.url !== afterReplace) {
        delete options[k];
        service.url = afterReplace;
      }
    }
    var ss = this.get(service);
    return this.http
      .request(
        new Request({
          method: ss.type,
          url: ss.url,
          headers: new Headers({
            Accept: "text/html;charset=UTF-8"
          }),
          body: ss.type != "GET" ? options : {},
          params: service.type == "GET" ? options : {}
        })
      )
      .map(res => res.json())
      .toPromise();
  }

  private get(service) {
    return { url: this.url + service.url, type: service.type };
  }
}
