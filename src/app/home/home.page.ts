import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { SMS } from "@ionic-native/sms/ngx";
import { NetworkService } from "./../services/network.service";
import { Component } from "@angular/core";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  info: string = "";
  cause: any = null;
  intervalTime = 30000; //30 seconds
  intervalCallback: any;
  constructor(
    public api: NetworkService,
    private sms: SMS,
    private androidPermissions: AndroidPermissions,
    private socialSharing: SocialSharing
  ) {}

  ngOnInit() {
    this.androidPermissions.requestPermission(
      this.androidPermissions.PERMISSION.SMS
    );
    this.callApi();
    this.intervalCallback = setInterval(() => {
      this.callApi();
    }, this.intervalTime);
  }

  ngOnDestroy() {
    clearInterval(this.intervalCallback);
  }

  shareSocial(provider) {
    switch (provider) {
      case "twitter": {
        this.socialSharing.shareViaTwitter(
          "This is some dummy text for testing twitter.."
        );
        break;
      }
      case "fb": {
        this.socialSharing.shareViaFacebookWithPasteMessageHint(
          "This is some dummy text for testing over facebook..",
          null,
          null,
          "Tap on paste"
        );
        break;
      }
      case "any": {
        this.socialSharing.shareWithOptions({
          message: "This is some dummy text for testing over any app",
          chooserTitle: "Choose a file to share"
        });
        break;
      }
    }
  }

  sendMSG() {
    this.androidPermissions
      .checkPermission(this.androidPermissions.PERMISSION.SMS)
      .then(
        result => {
          console.log("Has permission?", result.hasPermission);
          let smsObj = this.cause.organization.paymentMethods.SMS[0];
          var options = {
            replaceLineBreaks: false,
            android: {
              intent: "INTENT"
            }
          };
          this.sms.send(smsObj["-shortcode"], smsObj["-keyword"], options);
        },
        err =>
          this.androidPermissions.requestPermission(
            this.androidPermissions.PERMISSION.SMS
          )
      );
  }

  callApi() {
    this.api
      .send("testHttp", { country: "AE" })
      .then(res => {
        let causes = res.ikhair.country[0]["-causes"].cause;
        this.cause = causes[0];
        console.log(this.cause);
      })
      .catch(err => {
        console.log("error: ", err);
      });
  }
}
