import { NetworkService } from "./services/network.service";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
// import { HTTP } from "@ionic-native/http/ngx";
import { HttpModule } from "@angular/http";
import { NgCircleProgressModule } from "ng-circle-progress";
import { SMS } from "@ionic-native/sms/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      backgroundColor: "#c0c0c0",
      backgroundGradientStopColor: "#a8ffff",
      backgroundOpacity: 0,
      backgroundStroke: "#800040",
      backgroundStrokeWidth: 0,
      space: -20, //15
      toFixed: 0,
      maxPercent: 100,
      outerStrokeWidth: 30,
      outerStrokeColor: "#88d9ff",
      outerStrokeGradientStopColor: "#800080",
      outerStrokeLinecap: "square",
      innerStrokeColor: "#555555",
      innerStrokeWidth: 8,
      titleColor: "#5badff",
      showSubtitle: false,
      clockwise: false,
      titleFontSize: "25",
      titleFontWeight: "400",
      unitsFontSize: "23",
      unitsColor: "#5badff"
    })
  ],
  providers: [
    StatusBar,
    SMS,
    HttpModule,
    SplashScreen,
    NetworkService,
    AndroidPermissions,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
